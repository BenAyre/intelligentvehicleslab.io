---
title: Build Steps
layout: page
---

***Need check this when these work has been completed***

__--Need to check this (word or numbers) when the lab/office opens again--__


Note: could drill down the mounting holes on the trolley for the external frame. It is the easiest to do this at the first stage (the bottom panel/base of the trolley can be removed to be drilled so that there is less ‘blow-out’ on the underside of the board).


## [toc]


{% include toc.html html=content sanitize=true class="inline_toc" id="my_toc" %}

# 1. Build external frame – 40x40 Rexroth

**Bill of Materials**


## Connectors and Hardware

* 4 x Foundation Bracket - Mfr. Part No. 3842551605 <span style="color:blue">[LINK]( https://uk.rs-online.com/web/p/connecting-components/0523332/)</span>
 
* 4 x 3-way Connector for 40mm profile 10mm groove - Mfr. Part No. 3842549868 <span style="color:blue">[LINK](https://uk.rs-online.com/web/p/connecting-components/7982041/)</span>
 
* 10 x Strut profile t-connector - Mfr. Part No. 3842532195 <span style="color:blue">[LINK](https://uk.rs-online.com/web/p/connecting-components/7982066/)</span>

* 2 x 2-way Connector for 40mm profile 10mm groove -  Mfr. Part No. 3842549866 <span style="color:blue">[LINK](https://uk.rs-online.com/web/p/connecting-components/7982047/)</span>
 
* 24 x Bosch Rexroth Strut Profile T-Slot Nut, , M6 Thread strut profile 40 mm, 45 mm, 50 mm, 60 mm, Groove Size 10mm - Mfr. Part No. 3842530285 <span style="color:blue"> [LINK]( https://uk.rs-online.com/web/p/connecting-components/3900278/)</span>
 

## Struts

* 4 x 40x40 BR at 1020mm long  

* 2 x 40x40 BR at 840mm long

* 2 x 40x40 BR at 670mm long

* 2 x 40x40 BR at 640mm long

* 2 x 40x40 BR at 222mm long

* 2 x 40x40 BR at 160mm long 

* 1 x 40x40 BR at 185mm long 

For order;
* 3 x 3000mm length <span style="color:blue">[LINK](  https://uk.rs-online.com/web/p/tubing-struts/4597211/)</span>
* 1 x 2000mm length <span style="color:blue">[LINK]( https://uk.rs-online.com/web/p/tubing-struts/4597205/)</span>

## Panelling
Perforated Stainless steel – custom design M6 clearance holes as per CAD design above. Outsourced; to Laser cutting company at approximately £300.

## LiDAR Mount
Custom design in approx. 3mm Aluminium sheet as per CAD design above (2.5mm used for RSU 1 due to availability)

![LiDAR plate](/Mobile_RSU_pictures/wiki_pic/15_LiDAR_plate.jpg){:" height="800px"}


# 2. Build internal frame – 30x30 Rexroth

**Bill of Materials**

## Feet
* 4 x M8 threaded rod with swivel/pivot attached (comes with counter nut) 25mm rod <span style="color:blue">[LINK](  http://www.aluminium-profile.co.uk/acatalog/Trapezoid_Base.html)</span>

* 4 x M8 threaded rod with fixed feet attached (comes with counter nut) - Mfr. Part No. 3842502257 <span style="color:blue">[LINK]( https://uk.rs-online.com/web/p/flanges-feet-castors/3902230/ )</span>


## Connectors and Hardware
* 8 x Bosch Rexroth Strut Profile Angle Bracket, strut profile 30 x 60 mm, Groove Size 8mm - Mfr. Part No. 3842523541 <span style="color:blue">[LINK]( https://uk.rs-online.com/web/p/connecting-components/4371479/ )</span>

* 2 x bags of 10 Bosch Rexroth Strut Profile T-Slot Nut, M6 Thread strut profile 30 mm, Groove Size 8mm (16 needed) - Mfr. Part No. 3842501753 <span style="color:blue">[LINK]( https://uk.rs-online.com/web/p/connecting-components/3900256/)</span>


## Struts
* 2 x 925mm lengths - Bosch Rexroth Aluminium Strut, 30 x 30 mm, 8mm Groove

* 2 x 885mm lengths - Bosch Rexroth Aluminium Strut, 30 x 30 mm, 8mm Groove 

For order;

* 1 x 3000mm length <span style="color:blue">[LINK]( https://uk.rs-online.com/web/p/tubing-struts/3899796/)</span>

* 1 x 1000mm length <span style="color:blue">[LINK]( https://uk.rs-online.com/web/p/tubing-struts/4938252/  )</span>

 
## Panelling
3mm alloy sheet. Water jet to size according to CAD files above at the cost of £20 for two shelves (excluding material cost)

# 3. Mount Camera pole to rexroth
leave enough room for the pole to be easily removed. Use half coupler with natural rubber to grip the pole onto the Rexroth. (need to swap the bolt linking to the Rexroth, the bolt it comes with is too big in diameter and length. Use M8 hex socket cap screw 25mm with 13mm OD M8 washer). **//Drill through camera pole (M6 size), this then allows the L bracket to be mounted//**  <span style="color:blue">[LINK](  https://www.edmundoptics.co.uk/p/metric-type-large-size-right-angle-bracket/12225/))</span>

![Camera Pole](/Mobile_RSU_pictures/wiki_pic/7_Camera_pole.jpg){:" height="800px"}
# 4. Mount down rexroth to trolley
the bolt used here is the bolt came with the half coupler which is M10 size

![Rexroth trolley bracket](/Mobile_RSU_pictures/wiki_pic/12_Rexroth_trolley_bracket.jpg){:" width="1000px"}
# 5. IP rated box
* Drill through 3 M25 holes for 2 Ethernet (M25: max 13, min 6, RS 237-091) and one LiDAR (M25: Max 17, min 8.5, RS 365-8444)
* ***Double-sided tape box onto the cabinet, drill through an M32 hole.***

# 6. Power System
## Fan
Two self-tapping screws onto the side panel. Input goes into 1 and output to fan on 4. ***Need to add a picture of fan manual.***

![Thermometer](/Mobile_RSU_pictures/wiki_pic/11_thermometer.jpg){:" height="800px"}

## Battery tray
use 4 self-taping screws (N°8 x 3/8in Long 9.5mm Long) on each corner of the tray.

![Battery tray](/Mobile_RSU_pictures/wiki_pic/1_battery_tray.jpg){:" height="800px"}

## Shunt
The self-taping screws we have is too small, use a drill to drill enough for __--25mm M6 hex cap bolt to go through.--__

![Regulator](/Mobile_RSU_pictures/wiki_pic/2_Regulator.jpg){:" height="800px"}

## Regulator
Shown on the above picture. 3D printed 4 L bracket for the self-tapping screw (N°8 x 3/8in Long 9.5mm Long) onto base and __--M4 into the regulator--__ 
## Inverter
3D printed 3 L bracket for self-tapping screw onto the base (N°8 x 3/8in Long 9.5mm Long) 

![Inverter](/Mobile_RSU_pictures/wiki_pic/9_Inverter.jpg){:" width="1000px"}

## Charger
Use 4 self-tapping screws (N°8 x 3/8in Long 9.5mm Long) on each corner.

![Charger](/Mobile_RSU_pictures/wiki_pic/8_Charger.jpg){:" width="1000px"}

## Mount Shelves
This is waterjet cut to size. 8 M6 bolt on each shelf allow the shelf to be mounted onto L bracket at each corner of the shelf. 

![Shelf](/Mobile_RSU_pictures/wiki_pic/4_shelf.jpg){:" height="800px"}

## BMS cut off
custom made (laser cut) into the correct size. Left-hand side: 2 M6 bolt onto a L bracket. Right-hand side: M6 (15mm long we currently have 25mm which is too long) onto a t-slot nut.

![BMS plate off](/Mobile_RSU_pictures/wiki_pic/26_BMS_plate_off.jpg){:" width="1000px"}

## Heater
Once again, use the self-taping screw to mount it next to the thermometer. 

![Heater](/Mobile_RSU_pictures/wiki_pic/17_Heater.jpg){:" width="1000px"}

# ***Fuse Board***
